import React from 'react';
import data from './data.json';
import Products from './components/products';
import Filter from './components/filter';
import Cart from './components/cart';
class App extends React.Component {
  constructor(){
    super();
    this.state = {
      products: data.products,
      size: "",
      sort: "",
      cartItems: localStorage.getItem("cartItems") ? JSON.parse(localStorage.getItem("cartItems")) : [],
    }
  }

  sortProducts=(sortProduct)=>{
    const sort = sortProduct.target.value
    this.setState((state) => ({
      sort: sort,
      products: this.state.products.slice().sort((a,b) => (
        sort === "lowest"?
        ((a.price > b.price)? 1:-1):
        sort === "highest"?
        ((a.price < b.price)? 1:-1):
        ((a._id > b._id)? 1:-1)
      ))
    }))
  }

  filterProducts=(size)=>{
    if(size.target.value === ""){
      this.setState({size: size.target.value, products: data.products})
    } else {
      this.setState({
        size: size.target.value,
        products: data.products.filter(product => product.availableSizes.indexOf(size.target.value) >= 0)
      })
    }
  }

  removeFromCart=(product)=>{
    const cartItems = this.state.cartItems.slice();
    const newCartItems = cartItems.filter(x=>x._id !== product._id);
    this.setState({cartItems: newCartItems})
    localStorage.setItem("cartItems", JSON.stringify(newCartItems));
  }

  addToCart=(product)=>{
    const cartItems = this.state.cartItems.slice();
    let alreadyInCart = false;
    cartItems.forEach(cartItem => {
      if(cartItem._id === product._id) {
        cartItem.count++;
        alreadyInCart = true
      }
    });
    if(!alreadyInCart){
      cartItems.push({...product, count: 1})
    }

    this.setState({cartItems});

    localStorage.setItem("cartItems", JSON.stringify(cartItems));
  }

  createOrder = (order) => {
    alert("need to save" + order.name)
  }

  render(){
    return (
      <div className="grid-container">
        <header>
          <a href="/">RT Shopping Cart</a>
        </header>
        <main>
          <div className="content">
            <div className="main">
              <Filter
                count={this.state.products.length}
                size={this.state.size}
                sort={this.state.sort}
                filterProducts={this.filterProducts}
                sortProducts={this.sortProducts}
              />
              <Products products={this.state.products} addToCart={this.addToCart} />
            </div>
            <div className="sidebar">
              <Cart cartItems={this.state.cartItems} removeFromCart={this.removeFromCart} createOrder={this.createOrder} />
            </div>
          </div>
        </main>
        <footer>All right is reserved.</footer>
      </div>
    );
  }
}

export default App;
